package com.ig.core_backend_test_kit.test_sample_generic.item


import com.ig.core_backend.core.BaseRepository
import org.springframework.stereotype.Repository

@Repository
interface ItemRepository : BaseRepository<Item> {
}