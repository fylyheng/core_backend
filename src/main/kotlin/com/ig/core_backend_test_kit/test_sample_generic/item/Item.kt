package com.ig.core_backend_test_kit.test_sample_generic.item

import com.ig.core_backend.core.BaseEntity
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id


@Entity
data class Item (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long?=null,
    var name:String?=null,
    var price:Double?=0.0,
): BaseEntity()