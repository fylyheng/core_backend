package com.ig.core_backend_test_kit.test_sample_generic.test_new_feature

import jakarta.persistence.*
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Root
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.math.BigDecimal


data class ProductDTO(
    val name: String?,
    val category: String?,
)

@Entity
class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long? = null
    private val name: String? = null
    private val category: String? = null
    private val price: BigDecimal? = null
}


@Repository
interface ProductRepository : JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
//    @Query("SELECT p.name as name, p.category as category FROM Product p WHERE (:#{#spec.toPredicate(root, query, builder)})")
//    fun findProjected(spec: Specification<Product>): List<ProductDTO>
//    fun findAll(spec: Specification<Product>, projectionClass: Class<ProductDTO>): MutableList<ProductDTO>

}


@Service
class ProductService {
    @Autowired
    private lateinit var productRepository: ProductRepository
    @Autowired
    private lateinit var em : EntityManager

    fun test1(name: String?, category: String?): MutableList<Product> {
        var spec: Specification<Product> = Specification.where(null)
        if (name != null) {
            spec =
                spec.and { root: Root<Product?>, query: CriteriaQuery<*>?, builder: CriteriaBuilder ->
                    builder.like(root.get("name"), "%$name%")
                }
        }
        if (category != null) {
            spec =
                spec.and { root: Root<Product?>, query: CriteriaQuery<*>?, builder: CriteriaBuilder ->
                    builder.equal(
                        root.get<Any>("category"),
                        category
                    )
                }
        }

        return productRepository.findAll(spec)
    }


    fun test2(): MutableList<Product> {

        val cb: CriteriaBuilder = em.criteriaBuilder
        val cQuery: CriteriaQuery<Product> = cb.createQuery(Product::class.java)
        val root: Root<Product> = cQuery.from(Product::class.java)
        val paramEmpNumber = cb.parameter(String::class.java)

        cQuery
            .select(root)
            .where(cb.equal(root.get<String>("name"), paramEmpNumber))



        val query: TypedQuery<Product> = em.createQuery(cQuery)
        val empNumber = "A123"

        query.setParameter(paramEmpNumber, empNumber)
        val employee = query.resultList

        return employee!!
    }


    fun test3(specification: Specification<Product>): MutableList<ProductDTO> {
//        val data = productRepository.findAll { root, query, builder ->
//            val projectionResult = builder.construct(ProductDTO::class.java)
//
//            val person: QProduct = QProduct.product
//
//            val findMember: Product = queryFactory
//                .select(member)
//                .from(member)
//                .where(member.username.eq("member1"))
//                .fetchOne()
//        }


        return mutableListOf()
    }
}



