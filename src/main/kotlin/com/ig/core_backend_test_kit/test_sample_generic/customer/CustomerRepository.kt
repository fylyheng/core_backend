package com.ig.core_backend_test_kit.test_sample_generic.customer

import com.ig.core_backend.core.BaseRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerRepository : BaseRepository<Customer> {

//fun <R> findAll(specification: Specification<Customer>, projectionType: Class<R>, page: PageRequest): Page<R>
//    @Query("SELECT p FROM Product p where :filter")
//    fun findAllDynamicFilterAndProjection(@Param("filter") specification: Specification<Customer>)

}