package com.ig.core_backend_test_kit.test_sample_generic.customer

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service


@Service
class CustomerService : ICustomerService {

    @Autowired
    private lateinit var customerRepository: CustomerRepository


    override fun listCustomerDTO(allParams: Map<String, String>): Page<CustomerDTO> {

        val customName = allParams["customerName"]
        val invoiceNo = allParams["invoiceNo"]

        val page = allParams["page"]?.toInt() ?: 0
        val size = allParams["size"]?.toInt() ?: 10




//        fun findPeopleByLastName(lastName: String?): List<Customer> {
//            val c: QCustomer = QCustomer.customer
//            return jpaQueryFactory
//                .selectFrom<Any>(c)
//                .where(c.name.eq(lastName))
//                .fetch()
//        }





//        return customerRepository.findAll(
//            { root, query, cb ->
//                val predicates = ArrayList<Predicate>()
//
//                customName?.let {
//                    val cusId = cb.like(root.get("name"), "%${it.trim().toUpperCase()}%")
//                    predicates.add(cusId)
//                }
//
//                invoiceNo?.let {
//                    val saleSeries = cb.like(root.get("no"), "%${it.trim().toUpperCase()}%")
//                    predicates.add(saleSeries)
//                }
//
//                predicates.add(cb.isTrue(root.get("status")))
//                cb.and(*predicates.toTypedArray())
//            },
//            CustomerDTO::class.java,
//            PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"))
//        )

        return Page.empty()
    }

}


interface ICustomerService {
    fun listCustomerDTO(allParams: Map<String, String>): Page<CustomerDTO>
}