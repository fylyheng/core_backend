package com.ig.core_backend_test_kit.test_sample_generic.customer

import com.ig.core_backend.core.GenericRestfulController
import com.ig.core_backend.core.Slf4k.Companion.log
import com.ig.core_backend.core.exception.generalException.NotAcceptableException
import com.ig.core_backend.core.response.JSONFormat
import com.ig.core_backend.core.response.ResponseDTO
import com.ig.core_backend.utilities.AppConstant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(AppConstant.MAIN_PATH+"/customer")
class CustomerController : GenericRestfulController<Customer>(allowDelete = false){

    @Autowired
    private lateinit var jsonFormat: JSONFormat

    @Autowired
    private lateinit var customerService: ICustomerService


    override fun save(entity: Customer): Customer {
        entity.customerNo =  Random(4).nextInt()
        return super.save(entity)
    }

    override fun beforeSave(entity: Customer) {

        if (entity.customerNo!=null) {
            throw NotAcceptableException("customerNo must be null from client.")
        }
    }

    override fun afterSaved(entity: Customer) {
        log.info("writing log very detail here....")
        log.info("customerName :: ${entity.id}")
    }

    @PutMapping("{id}")
    override fun update(@PathVariable(value = "id") id: Long, @RequestBody entity: Customer?): ResponseDTO {
        val data = super.update(
            id,
            entity!!,
            fieldsProtected = listOf("customerNo","phoneNumber","id")
        )

        return jsonFormat.respondID(data)
        //return JSONFormat.respondObj(data)
    }

//    @GetMapping(AppConstant.LIST_PATH)
//    override fun list(@RequestParam allParams: Map<String, String>): ResponseDTO {
//        return super.list(allParams)
//    }
//
//
//    @GetMapping("/list-dto")
//    fun listCustomerDTO(@RequestParam allParams: Map<String, String>) : ResponseDTO {
//
//        val data = customerService.listCustomerDTO(allParams)
//        return JSONFormat.respondPage(data)
//
//    }
}