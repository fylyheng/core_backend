package com.ig.core_backend_test_kit.test_sample_generic.customer

import com.ig.core_backend.core.BaseEntityRequestDTO
import org.springframework.stereotype.Component

@Component
data class CustomerRequestDto (
    var name : String?,
    var age : Int
) : BaseEntityRequestDTO()