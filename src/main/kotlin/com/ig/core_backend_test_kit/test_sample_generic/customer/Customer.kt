package com.ig.core_backend_test_kit.test_sample_generic.customer

import com.ig.core_backend.core.BaseEntity
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id


@Entity
data class Customer(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long?=null,
    var customerNo:Int?=null, //auto increase

    var name:String?=null,
    var phoneNumber:String?=null,

) : BaseEntity()
