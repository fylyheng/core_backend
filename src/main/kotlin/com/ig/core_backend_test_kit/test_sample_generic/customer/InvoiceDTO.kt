package com.ig.core_backend_test_kit.test_sample_generic.customer


interface InvoiceDTO {

    //var invoiceNo:String?
    var customerName:String?
    var id:Long?
    //var grandTotal:Double?
    var invoiceDetail : MutableList<InvoiceDetailDTO>?

}

interface InvoiceDetailDTO {
    //var invoiceId:Long?
    var price: Double?
    //var item : ItemDTO?
}

interface ItemDTO {
    var name :String ?
}