package com.ig.core_backend_test_kit.test_sample_generic.customer

interface CustomerDTO {

    var name:String?
    var phoneNumber:String?
}