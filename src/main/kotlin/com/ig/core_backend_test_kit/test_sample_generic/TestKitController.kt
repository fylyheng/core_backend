package com.ig.core_backend_test_kit.test_sample_generic


import com.google.gson.Gson
import com.ig.core_backend.core.Slf4k.Companion.log
import com.ig.core_backend.core.response.JSONFormat
import com.ig.core_backend.core.response.ResponseDTO
import com.ig.core_backend.utilities.AppConstant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(AppConstant.MAIN_PATH+"/core/info/test")
class TestKitController {
    @Autowired
    private lateinit var json : JSONFormat
    @Autowired
    private lateinit var gson: Gson


    @GetMapping
    fun getInfo() : ResponseDTO {

        val data = mapOf(
            "coreApplication" to "CoreBackend",
            "version" to "1.1.0"
        )
        log.info(gson.toJson(data))

        return json.respondObj(data)
    }
}