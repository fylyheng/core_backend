package com.ig.core_backend_test_kit.test_sample_generic.invoice

import com.fasterxml.jackson.annotation.JsonIgnore
import com.ig.core_backend.core.BaseEntity
import com.ig.core_backend_test_kit.test_sample_generic.item.Item
import jakarta.persistence.*


@Entity
data class InvoiceDetail(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long?,
    var qty:Int?=0,
    var price:Double?=0.0,

    @OneToOne(fetch = FetchType.LAZY)
    var item: Item?,

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id", insertable = true, updatable = false,nullable = true)
    var invoice : Invoice?= null


): BaseEntity()
