package com.ig.core_backend_test_kit.test_sample_generic.invoice


import com.ig.core_backend.core.BaseRepository
import org.springframework.stereotype.Repository

@Repository
interface InvoiceRepository: BaseRepository<Invoice>