package com.ig.core_backend_test_kit.test_sample_generic.invoice

import com.ig.core_backend_test_kit.test_sample_generic.customer.InvoiceDTO
import org.springframework.data.domain.Page

interface InvoiceService {

    fun testThrow(i:Long)
    fun findAllList(allParams: Map<String, String>?): Page<InvoiceDTO>
}