package com.ig.core_backend_test_kit.test_sample_generic.invoice

import com.ig.core_backend.core.GenericRestfulController
import com.ig.core_backend.core.Slf4k
import com.ig.core_backend.core.Slf4k.Companion.log
import com.ig.core_backend.core.exception.entityExecption.NotFoundException
import com.ig.core_backend.core.exception.generalException.NotAcceptableException
import com.ig.core_backend.core.response.ResponseDTO
import com.ig.core_backend.utilities.AppConstant
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

@Slf4k
@RestController
@RequestMapping(AppConstant.MAIN_PATH+"/invoice")
class InvoiceController : GenericRestfulController<Invoice>(allowUpdate = false, allowDelete = false){

    @Autowired @Qualifier("invV2")
    lateinit var invoiceService: InvoiceService



    @GetMapping("/throw")
    fun testThrow(@RequestParam i : Long): ResponseDTO {

        if (i == 10L) {
            invoiceService.testThrow(10)
        }

        else if (i <0){
            throw RuntimeException("I can't not be negative")
        }

        else if (i == 30L){
            throw NotFoundException("custom not found exp I is ${30L}")
        }

        return JSONFormat.respondDynamic(data = i,  HttpStatus.OK, HttpStatus.OK.reasonPhrase , 0 )
    }


    @PutMapping("/{id}")
    override fun update(@PathVariable(value = "id") id: Long, @RequestBody entity: Invoice?): ResponseDTO {
        log.warn("update invoice hit...")

        val data = repo!!.getReferenceById(id)

        utilService.bindProperties(entity!!, data, exclude = listOf("invoiceNo","invoiceDetail"))

        data.invoiceDetail?.clear()
        data.invoiceDetail?.addAll(entity.invoiceDetail?: listOf())


        return JSONFormat.respondObj(super.save(data))
    }


    @PostMapping
    override fun create(@RequestBody entity: Invoice): ResponseDTO {

        this.beforeSave(entity)


        entity.invoiceNo = "inv-000001"
        entity.date = Date()
        val result = super.save(entity)

        this.afterSaved(entity)

        return JSONFormat.respondID(result)
    }



    override fun beforeSave(entity: Invoice) {
        if (entity.customerName == null) {
            throw NotAcceptableException("Customer name Can Not Be Null")
        }
    }

    override fun afterSaved(entity: Invoice) {
        log.info("I can do something in this block before response to Client")
    }



    @GetMapping("/${AppConstant.LIST_DTO_PATH}")
    @ApiImplicitParams(value = [
        ApiImplicitParam(name = "customerId", required = false, paramType = "query"),
        ApiImplicitParam(name = "customerName",required = false, paramType = "query")
    ])
    fun listCriteriaWithProjection(@RequestParam allParams: Map<String, String>): ResponseDTO {

        val data = invoiceService.findAllList(allParams)
        return JSONFormat.respondPage(data)
    }

}