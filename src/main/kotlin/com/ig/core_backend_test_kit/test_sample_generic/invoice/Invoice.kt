package com.ig.core_backend_test_kit.test_sample_generic.invoice

import com.ig.core_backend.core.BaseEntity
import jakarta.persistence.*
import java.util.Date

@Entity
data class Invoice(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long?=null,

    var invoiceNo:String?=null,
    var date: Date?,
    var customerName:String?,
    var subTotal:Double?,
    var grandTotal:Double=0.0,
    var vatAmount:Double?,


    @OneToMany(
        targetEntity = InvoiceDetail::class, cascade = [CascadeType.ALL],
        fetch = FetchType.LAZY, orphanRemoval = true
    )
    @JoinColumn(name = "invoice_id",referencedColumnName = "id")
    var invoiceDetail: MutableList<InvoiceDetail>?=null,


    ): BaseEntity()
