package com.ig.core_backend_test_kit

import com.ig.core_backend.AutoConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories
@Import(AutoConfig::class)
class IGCoreBackendApplication

fun main(args: Array<String>) {
	runApplication<IGCoreBackendApplication>(*args)
}
